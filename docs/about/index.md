
## My background

I am 26 years old and live in Arequipa, one of the most beautiful cities in Peru. I am passionate and interested in everything that can be created with mechanics and technology together. 

## Previous work

I graduated in Plant Machinery Maintenance from the Tecsup Institute. In the year 2023 I will graduate from the career of Mechanical Engineering from Universidad Continental.
After graduating, I worked in design and manufacturing of mechanical and structural components using CAD and CAE software.

Currently I teach courses related to design, assisted manufacturing and control of mechanisms.

![](../images/Images-page/foto-trabajo.jpg){width=653, length=237}

Design and positioning of dust suppressors. Antamina.

![](../images/Images-page/foto-tec.jpg){width=481, length=229}

Teaching courses at Tecsup.