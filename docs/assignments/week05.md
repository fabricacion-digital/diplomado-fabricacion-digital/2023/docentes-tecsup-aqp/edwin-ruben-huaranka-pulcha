# 5. 3D Scanning and printing

## Advantages and limitations of 3D printing

The printer used for this activity is the Crealuty CR-20 PRO, which has the following technical characteristics:

- Printing Area: 22x22x25cm
- Resolution: 0.05mm - 0.3mm
- Accuracy: +/-0.1mm
- Speed: ≤100mm/s
- Recommended working time: ≤72h
- Number of Nozzles: 1
- Nozzle outlet diameter: 0.4mm
- Filament Diameter: 1.75mm
- Materials: PLA, ABS, PETG, Wood, TPU*
- Extruder temperature: 255°C
- Hot bed temperature: 110°C

To start with the 3D printing, a series of tests were performed in different printing situations, for which 3D test objects were designed to verify the sizing and the inclination that the machine could print.

![](../images/tarea4/t4-00.jpg)

Design of object to check the sizing in Inventor.

![](../images/tarea4/t4-01.jpg)

Configuration of printing parameters in Cura software.

![](../images/tarea4/t4-02.jpg)

3D printed object.

![](../images/tarea4/t4-03.jpg)

Verification of object dimensions.

> Results of the dimensions obtained: For the exterior measurements it was identified that the impression is 0.1 mm oversized, for the interior measurements it was identified that the impression is 0.1 mm undersized and for the depth measurements there is no variation in the dimension.


![](../images/tarea4/t4-04.jpg)

Object design to check the printing inclination in Inventor.

![](../images/tarea4/t4-05.jpg)

Print parameters configuration in Cura software.

![](../images/tarea4/t4-06.jpg)

3D printed object without support.

![](../images/tarea4/t4-07.jpg)

3D printed object with support.

![](../images/tarea4/t4-08.jpg)

Verification of printing with different tilt angles without and with support.

> Results of the printing in angles: It was observed in the printing with support that from 35º to more it does not present inconveniences, at a lower angle it begins to present an uneven finish and the material is deformed. On the other hand, the printing with support avoids this inconvenience of the angle because it has an "aid" and avoids the deformation, but it was observed that at an angle of less than 15º the finish is not as good as it should be.


## Design and 3D print an object that could not be made subtractively

### Subtractive manufacturing

Subtractive manufacturing is a general term that encompasses various controlled machining and material removal processes. These processes begin with solid metal or plastic blocks, bars, or rods that are shaped by removing material from them through cutting, drilling, and grinding processes.

### Additive manufacturing

Unlike the subtractive process in which material is removed from a larger part, additive manufacturing or 3D printing processes build objects by adding material layer by layer, with each successive layer joining the previous one until the part is complete.

### Design and 3D print of the Object

For this activity a flange with split view was designed, to observe its internal shape, this component could not be manufactured with subtractive manufacturing due to the amount of details and different shapes it presents.

![](../images/tarea4/t4-09.jpg)

Design of the component in Solidworks software.

![](../images/tarea4/t4-10.jpg)

Configuration of printing parameters in Cura.

![](../images/tarea4/t4-11.jpg)

3D printed component with additive manufacturing.

## 3D scan an object

For this activity was scanned to a bench press and printed at a scale of 1:10.

![](../images/tarea4/t4-12.jpg)

Bench press to be scanned.

![](../images/tarea4/t4-13.jpg)

Component scanning process.

![](../images/tarea4/t4-14.jpg)

Finished scanning and detail generation in Sense software.

![](../images/tarea4/t4-15.jpg)

Configuration of printing parameters in Cura.

![](../images/tarea4/t4-16.jpg)

3D printed scaled component.

![](../images/tarea4/t4-17.jpg)

Comparison between the real component and the 3D scanned and printed one.

![](../images/tarea4/t4-18.jpg)

Both jobs were successfully completed!

## Useful links

- [Solidworks](https://www.solidworks.com/es)

- [Inventor](https://www.autodesk.com/products/inventor/overview?term=1-YEAR&tab=subscription&plc=INVPROSA)

- [Ultimaker Cura](https://ultimaker.com/software/ultimaker-cura/)

- [Sense](https://support.3dsystems.com/s/article/Sense-Scanner?language=en_US)


## Files

- [Test dimensions](../images/tarea4/files/test-dimension.rar)

- [Test angles](../images/tarea4/files/test-angle.rar)

- [3d printed component](../images/tarea4/files/3dprint-component.rar)

- [Scanned component](../images/tarea4/files/escaner-component.rar)
