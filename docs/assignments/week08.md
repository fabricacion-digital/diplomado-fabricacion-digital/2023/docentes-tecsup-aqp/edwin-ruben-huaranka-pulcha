# 8. Embedded programming

## Compare the performance and development workflows for other architectures

### ESP32-DevKitC

#### General description

ESP32-DevKitC V4 is a small-sized ESP32-based development board produced by Espressif. Most of the I/O pins are broken out to the pin headers on both sides for easy interfacing. Developers can either connect peripherals with jumper wires or mount ESP32-DevKitC V4 on a breadboard.

#### Functional Description

The key components, interfaces and controls of the ESP32-DevKitC V4 board are detailed in the following figure and table below.

![](../images/tarea8/t8-00.jpg)

ESP32-DevKitC V4 with ESP32-WROOM-32 module soldered

| Key Component | Description |
| --- | --- |
| ESP32-WROOM-32 | A module with ESP32 at its core. For more information, see ESP32-WROOM-32 Datasheet. |
| EN | Reset button. |
| Boot | Download button. Holding down Boot and then pressing EN initiates Firmware Download mode for downloading firmware through the serial port. |
| USB-to-UART Bridge | Single USB-UART bridge chip provides transfer rates of up to 3 Mbps. |
| Micro USB Port | USB interface. Power supply for the board as well as the communication interface between a computer and the ESP32-WROOM-32 module. |
| 5V Power On LED | Turns on when the USB or an external 5V power supply is connected to the board. For details see the schematics in Related Documents. |
| I/O | Most of the pins on the ESP module are broken out to the pin headers on the board. You can program ESP32 to enable multiple functions such as PWM, ADC, DAC, I2C, I2S, SPI, etc. |

#### Header Block

The two tables below provide the Name and Function of I/O header pins on both sides of the board, as shown in ESP32-DevKitC V4 with ESP32-WROOM-32 module soldered.

J2

![](../images/tarea8/t8-01.jpg)

J3

![](../images/tarea8/t8-02.jpg)

> P: Power supply; I: Input; O: Output.

> The pins D0, D1, D2, D3, CMD and CLK are used internally for communication between ESP32 and SPI flash memory. They are grouped on both sides near the USB connector. Avoid using these pins, as it may disrupt access to the SPI flash memory/SPI RAM.

> The pins GPIO16 and GPIO17 are available for use only on the boards with the modules ESP32-WROOM and ESP32-SOLO-1. The boards with ESP32-WROVER modules have the pins reserved for internal use.

#### Pin Layout

![](../images/tarea8/t8-03.jpg)

ESP32-DevKitC Pin Layout

### Arduino Mega 2560

#### General description

Arduino Mega 2560 is an exemplary development board dedicated for building extensive applications as compared to other maker boards by Arduino. The board accommodates the ATmega2560 microcontroller, which operates at a frequency of 16 MHz. The board contains 54 digital input/output pins, 16 analog inputs, 4 UARTs (hardware serial ports), a USB connection, a power jack, an ICSP header, and a reset button.

#### Functional Description

In the following image you can see the block diagram of the Arduino Mega

![](../images/tarea8/t8-04.jpg)

Arduino MEGA Block Diagram

#### Header Block

Analog

![](../images/tarea8/t8-05.jpg)

Digital

![](../images/tarea8/t8-06.jpg)

#### Pin Layout

![](../images/tarea8/t8-07.jpg)
![](../images/tarea8/t8-08.jpg)

Arduino Mega 2560 Pin Layout

### Differences between ESP32 and Arduino Mega

- The Arduino Mega is comparable to the Esp32 in terms of suitable applications, for example, wearables, automation, and robotics. However, the Arduino Mega is still missing Bluetooth and Wi-Fi connectivity without add-ons or upgrades. Furthermore, the cost of Arduino and its add-ons comes at a higher price when the features are compared to those of the Esp32.

- With that said, Arduino is the better choice for beginners since it also comes with plenty of material for STEM education. There are also many Arduino projects for engineering students and 3D printable Arduino project files, particularly robots, available.

- On the other hand, the Esp32 is energy efficient with wireless technology and can operate in rough conditions, with operating temperatures between -40 °C and 125 °C. If you need Wi-Fi or Bluetooth connectivity for a project, like IoT, Esp32 boards are the obvious, cost-effective choice, and they may be better suited for intermediate or advanced users who need a powerful and feature-packed option.

### Programming the NODEMCU with Arduino IDE

To start programming on the ESP32 microcontroller, we configure the Arduino IDE software with the necessary libraries.

![](../images/tarea8/t8-09.jpg)

Go to the preferences menu

In the "Additional boards manager URLs" box we add: https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json

![](../images/tarea8/t8-10.jpg)

URL added in the box.

![](../images/tarea8/t8-11.jpg)

Then we go to Tools>Board: ...>Boards manager

![](../images/tarea8/t8-12.jpg)

Type and search for ESP32, select the Espressif Systems library and install it.

The installation will take a short time and when finished the item ESP32 will appear as installed.

Now in Tools>>>Boards, the new board should be installed.

![](../images/tarea8/t8-13.jpg)

Select "Esp32 Dev module".

<iframe width="1250" height="500" src="https://www.youtube.com/embed/ifhmAglDpzE" title="Conexion ESP32" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Connection to ESP32.

## Write a program for a microcontroller development board to interface with input and/or output devices and communicate with remote wired or wireless devices.

To load a program into the ESP32 microcontroller the following steps must be followed:

![](../images/tarea8/t8-14.jpg)

Verify the connection of the microcontroller to the PC (COM3).

![](../images/tarea8/t8-15.jpg)

Select the correct board (ESP32 Dev Module).

![](../images/tarea8/t8-16.jpg)

Select the connection port (COM3).

Once all the above is done, we proceed to load the code, in this case we generated a program for a sequence of LED lights.

![](../images/tarea8/t8-17.jpg)

Code generated in Arduino IDE

![](../images/tarea8/t8-18.jpg)

Click on "Upload" button

![](../images/tarea8/t8-19.jpg)

In the lower part of the software appears the information of the program upload.

In my case, when the message "Connecting ..." appears, you must press the BOOT button of the ESP32 microcontroller to continue loading the program.

![](../images/tarea8/t8-20.jpg)

Final information of successful loading of the program.

Program code. Sequence of 3 LED lights.

```
#define verde 17             //Se definen los pines a los que están conectados los LEDs.
#define rojo 16
#define azul 4

void setup() {

  pinMode(verde,OUTPUT);     //Se configuran los pines como salida.
  pinMode(rojo,OUTPUT);
  pinMode(azul,OUTPUT);

}

void loop() {

  digitalWrite(verde,HIGH);  //high = 1    low = 0 
  delay(1000);               //Espera de un segundo.
  digitalWrite(rojo,HIGH);
  delay(1000);
  digitalWrite(azul,HIGH);
  delay(1000);
  digitalWrite(verde,LOW);
  delay(1000);
  digitalWrite(rojo,LOW);
  delay(1000);
  digitalWrite(azul,LOW);
  delay(1000);
}
```

![](../images/tarea8/t8-21.jpg)

3-LED sequence connection

<iframe width="1250" height="500" src="https://www.youtube.com/embed/s1XasLIjANg" title="Secuencia 3 led" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

3-LED sequence operation.

## Useful links

- [ESP32 Data](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/hw-reference/esp32/get-started-devkitc.html)

- [Arduino Mega Data](https://docs.arduino.cc/resources/datasheets/A000067-datasheet.pdf)

- [Arduino IDE](https://www.arduino.cc/en/software)

- [Using ESP32 with Arduino IDE](https://lastminuteengineers.com/esp32-arduino-ide-tutorial/)

## Files

- [Arduino ESP32 test](../images/tarea8/files/prueba_leds.ino)

