# 2. Project management

This session I worked on defining my final project idea and started to getting used to the documentation process.

## Software used

The project repository will be developed using the software GIT. The use of this software will be detailed below. 

### Software GIT

The GIT software will be used to link the files and coding created for the generation of the web page (project repository).

In order to download and install it it is necessary to access to its web page. [GIT PAGE](https://git-scm.com/download/win)

We proceed to download the installer for Windows 64 bit operating system.

![](../images/tarea1/t1-00.jpg)

Once downloaded the installer, we proceed to install the software.

This software allows us to clone the template found in FabAcademy to start with the personal repository, this operation is performed as follows.

Copy the HTTPS link of the template.

![](../images/tarea1/t1-01.jpg)

Run GIT by right clicking on an empty folder created.

![](../images/tarea1/t1-02.jpg)

Select "Open Git Bash here".

![](../images/tarea1/t1-03.jpg)

In the interface type "git clone" and copy the template address. Finish with "Enter".

![](../images/tarea1/t1-04.jpg)

![](../images/tarea1/t1-05.jpg)

Immediately a folder will be generated, which is the copy of the template.

![](../images/tarea1/t1-06.jpg)

We access to that folder, there we will find the base files for editing the repository.

![](../images/tarea1/t1-07.jpg)

But before we can edit we must have a link between this folder and the repository on the web, for which within the folder we must right click and select "Open Git Bash Here".

![](../images/tarea1/t1-08.jpg)

We proceed to generate a graphical password, for which we will type "ssh-keygen -t rsa -b 2048 -C "email used"" and press "Enter".

![](../images/tarea1/t1-09.jpg)

A password is assigned and confirmed, press "Enter". And now the graphic password appears.

![](../images/tarea1/t1-10.jpg)

This password must be obtained but in text mode, for which we will type "cat ~/.ssh/id_rsa.pub" and press "Enter".

![](../images/tarea1/t1-11.jpg)

The textual password appears, which will be added to the web to make the link.

![](../images/tarea1/t1-12.jpg)

![](../images/tarea1/t1-13.jpg)

Once all this is done, we have the link of the folder with the web page (repository of the project).

The last thing that must be done so that any edition or increase of information (images, videos, text, etc) is visible is:

- Save any changes made and open the GIT software, type "git status", if red text appears it indicates that there are changes.

![](../images/tarea1/t1-14.jpg)

- Type "git add ." and press "Enter".

- Type "git commit -m "modification name"" and press "Enter".

- Type "git push" and press "Enter".

![](../images/tarea1/t1-15.jpg)

In my case after each "git push" it asks me to access my user and password already configured in the page.

![](../images/tarea1/t1-16.jpg)

![](../images/tarea1/t1-17.jpg)

In this way all the edits made will be updated in the web page (project repository).