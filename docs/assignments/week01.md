# 1. Principles and practices

This session I worked on defining my final project idea and started to getting used to the documentation process.

## Research

Robotic arms are fast, reliable and accurate, and can be programmed to perform an infinite number of tasks in a variety of environments. They are used in factories to automate the execution of repetitive tasks, for example, applying paint to equipment or parts; in warehouses to pick, select or sort products on distribution conveyors to fulfill consumer orders; or in an agricultural field to pick ripe fruit and place it in storage trays. In addition, as robotic technologies are developed and industrial environments become more connected, the capabilities of robotic arms are expanding to enable new use cases and business operating models.

Industrial robotic arms help companies increase their competitive advantage and keep costs low by enabling automation of key processes, which contribute to increased worker safety, accelerated production and improved productivity.

### Robotic arm uses

Automating these types of tasks not only removes human workers from potentially dangerous situations, but allows those workers to perform high-value tasks, such as interacting with customers. Here are some of the most common ways manufacturers are using robotic arms today:

- Palletizing
- Material handling
- Welding
- Inspection
- Pick and place


> One of the key advantages of industrial robotic arms is their versatility to support a variety of uses, from the simplest to the most complex jobs in the safest to the most demanding environments.

## Purpose of the final project

Develop a robotic arm that can be applied in different branches of industry.

![](../images/proyecto/boceto-brazo2.jpg)

Prototype of the Robotic Arm


## What will it do?

The Robotic Arm will have different axes of movement, which depending on the application that you want to give the programming of it will be different; the movement of the joints will be performed by servomotors, which will provide the option to regulate the speed or drive force.

## Who will use it?

Mainly the Robotic Arm will be manufactured for its implementation in a Robotino to increase its application in education. In the same way it can be used by students of various technical careers, to understand the operation of components such as servo motors, microcontrollers and to be able to link it with equipment of the same type or control.