# 11. Output devices

## Measure the power consumption of an output device

Measurements were performed with a servo motor as the output device.

The SG90 mini servo motor is a small rotary actuator or motor that allows precise control in angular position, this servo motor can rotate from 0° to 180°, its operating voltage ranging from 4.8 to 6 VDC.

![](../images/tarea11/t11-00.jpg)

SG90 servo motor.

![](../images/tarea11/t11-01.jpg)

Servo motor PWM connection and diagram.

Current measurement was performed on the motor being controlled by a potentiometer.

![](../images/tarea11/t11-02.jpg)

Current measurement indicating the maximum value, 204 mA.

![](../images/tarea11/t11-03.jpg)

Current measurement indicating the minimum value, 2 mA.

<iframe width="1250" height="500" src="https://www.youtube.com/embed/JlFDClZu6Z8" title="Corriente servo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Servo motor current measurement.

The voltage consumed by the motor was obtained with the use of the oscilloscope, and the voltage consumed by the potentiometer at its maximum and medium operation was also visualized.

![](../images/tarea11/t11-04.jpg)

Measurement of minimum voltage consumed: Servo motor = 3.3v, Potentiometer = 0v.

![](../images/tarea11/t11-05.jpg)

Measurement of maximum voltage consumed: Servo motor = 3.3v, Potentiometer = 5v.

<iframe width="1250" height="500" src="https://www.youtube.com/embed/4svduj8l3yI" title="Servo Potenciometro Osciloscopio" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Measurement of voltage consumed in servo motor and potentiometer with oscilloscope.

![](../images/tarea11/t11-06.jpg)

Calculation of servo motor power consumption, 673.2 mW.

## Add an output device to a microcontroller board you’ve designed, and program it to do something

We test the operation of 4 servo motors with pushbuttons

### Libraries used

To control the servo motor with the ESP32 microcontroller we need the "ESP32Servo.h" library. We must install this library with the following steps.

![](../images/tarea11/t11-07.jpg)

First, open our Arduino IDE and go to Sketch > Include Library > Manage Libraries. The Library Manager should open.

![](../images/tarea11/t11-08.jpg)

Type "Esp32servo" in the search box and install the ESP32Servo library in my case from Kevin Harrington.

Finally, restart the Arduino IDE software.

### Running code for 4 servo motors

Once we have installed the necessary library to run the servo motors, we proceed to generate the code in the software and upload it to the ESP32 microcontroller.

![](../images/tarea11/t11-09.jpg)

Code generated in the Arduino IDE software

The code is as follows:

```
#include <ESP32Servo.h>
Servo servo1;               //base
Servo servo2;               //brazo
Servo servo3;               //antebrazo
Servo servo4;               //garra

int pin_izquierda = 16;
int pin_derecha =   4;
int pin_arriba =    18;
int pin_abajo =     5;
int pin_garra =     17;

int var_izquierda = 0;
int var_derecha = 0;
int var_arriba = 0;
int var_abajo = 0;
int var_garra = 0;
boolean flag_garra = false;

int pin_servo1 = 13;
int pin_servo2 = 12;
int pin_servo3 = 14;
int pin_servo4 = 27;

int pos_servo1 = 90;
int pos_servo2 = 90;
int pos_servo3 = 90;
int pos_servo4 = 0;

int tiempo_servo = 20;

void setup() {
  Serial.begin(9600);           //Iniciamos comunicación

  pinMode(pin_izquierda, INPUT); 
  pinMode(pin_derecha, INPUT);
  pinMode(pin_arriba, INPUT);
  pinMode(pin_abajo, INPUT);
  pinMode(pin_garra, INPUT); 
  
  pinMode(pin_servo1, OUTPUT);
  pinMode(pin_servo2, OUTPUT);
  pinMode(pin_servo3, OUTPUT);
  pinMode(pin_servo4, OUTPUT);
  servo1.attach(pin_servo1);
  servo2.attach(pin_servo2);
  servo3.attach(pin_servo3);
  servo4.attach(pin_servo4);
  
  servo1.write(pos_servo1);
  servo2.write(pos_servo2);
  servo3.write(pos_servo3);
  servo4.write(pos_servo4);        //Iniciamos con la garra abierta
  delay(2000);
}

void loop() {

  var_izquierda = digitalRead(pin_izquierda);
  var_derecha = digitalRead(pin_derecha);
  var_arriba = digitalRead(pin_arriba);
  var_abajo = digitalRead(pin_abajo);
  var_garra = digitalRead(pin_garra);

  if(var_izquierda == HIGH){
    izquierda();  
  }
  if(var_derecha == HIGH){
    derecha();  
  }
  if(var_arriba == HIGH){
    arriba();  
  }
  if(var_abajo == HIGH){
    abajo();  
  }

  if(var_garra == HIGH && flag_garra == false){
    cerrar_garra();  
    flag_garra = true;
  }

  if(var_garra == HIGH && flag_garra == true){
    abrir_garra();  
    flag_garra = false;
  }
 
  delay(50);
 
  }
  
  void izquierda(){
    servo1.write(pos_servo1);
    delay(tiempo_servo);
    pos_servo1 = pos_servo1 + 1; 
    if(pos_servo1>=180){
      pos_servo1 = 180;
    }
     
  }
  
  void derecha(){
    servo1.write(pos_servo1);
    delay(tiempo_servo);
    pos_servo1 = pos_servo1 - 1;
    if(pos_servo1<=0){
      pos_servo1 = 0;
    }
  }
  
  void arriba(){
    servo2.write(pos_servo2);
    servo3.write(pos_servo3);
    delay(tiempo_servo);
    pos_servo2 = pos_servo2 + 1;
    pos_servo3 = pos_servo3 - 1;
    if(pos_servo2>=120){
      pos_servo2 = 120;
      pos_servo3 = 60;
    }
  }
  
  void abajo(){
    servo2.write(pos_servo2);    
    delay(tiempo_servo);
    pos_servo2 = pos_servo2 - 1;
    if(pos_servo2<=0){
      pos_servo2 = 0;  
    }    
  }
  
  void abrir_garra(){   
    while(var_garra == HIGH){
     servo4.write(pos_servo4);
     delay(tiempo_servo);
     pos_servo4 = pos_servo4 - 1;
     var_garra = digitalRead(pin_garra);
     if(pos_servo4<=0){
      pos_servo4 = 0;
     } 
    }
  }
  
  void cerrar_garra(){    
    while(var_garra == HIGH){                        
     servo4.write(pos_servo4);
     delay(tiempo_servo);
     pos_servo4 = pos_servo4 + 1;
     var_garra = digitalRead(pin_garra);  
     if(pos_servo4>=180){
      pos_servo4 = 180;
     }   
    }   
  }
```
### PCB layout for wiring

![](../images/tarea9/t9-11.jpg)

Schematic circuit for push buttons.

![](../images/tarea9/t9-13.jpg)

Circuit board for push buttons.

![](../images/tarea9/t9-14.jpg)

PCB with soldered components.

### Operation of 4 servo motors with 5 push buttons

![](../images/tarea11/t11-10.jpg)

Servo motors connected to the pushbuttons and the microcontroller.

<iframe width="1250" height="500" src="https://www.youtube.com/embed/DIKC2M5Yb24" title="Servo motor pulsadores" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Servo motor operation.

## Useful links

- [SG90 Mini Servo Motor](https://www.mpja.com/download/31002md%20data.pdf)

- [Servo ESP32 Arduino](https://www.arduino.cc/reference/en/libraries/servo/)

## Files

- [Servo motor test](../images/tarea11/files/Prueba_servo_pot.ino)

- [Servo motor control](../images/tarea11/files/Brazo_control_pulsadores.ino)

