# 9. Electronics design

## Use the test equipment in your lab to observe the operation of a microcontroller circuit board

### LED operation

![](../images/tarea9/t9-00.jpg)

Visualization of LED operation with a 1-second on/off sequence.

<iframe width="1250" height="500" src="https://www.youtube.com/embed/3uKAuHZ5WlI" title="Led osciloscopio" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Oscilloscope display of LED operation with reset pulse.

### Servo motor operation with PWM signal.

![](../images/tarea9/t9-01.jpg)

Display of the PWM signal for minimum operation.

![](../images/tarea9/t9-02.jpg)

Display of the PWM signal at maximum operation.

<iframe width="1250" height="500" src="https://www.youtube.com/embed/wcYrQSv6AY0" title="Servo osciloscopio" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Oscilloscope display of servo motor operation with PWM signal.

## Design a development board using Electronic Design Aided Software (EDA) to interface and communicate with an embedded microcontroller board.

### PCB for ESP32 microcontroller

A general purpose circuit board was designed for the ESP32 microcontroller, the design was done in Eagle software.

![](../images/tarea9/t9-06.jpg)

Schematic circuit for the ESP32 microcontroller.

We switched from schematic mode to board mode.

![](../images/tarea9/t9-07.jpg)

The added components are displayed in an unordered way.

We arrange the components according to the required need. For the process of connecting the components we used the following tools.

![](../images/tarea9/t9-08.jpg)

Tools used for the connection of components.

1. Add components to the schematic panel.
2. Add new libraries or components to the software.
3. Generate the CAM process for manufacturing.
4. Generate the connections or routes between components.
5. Generate a free form, used for GND.
6. With the help of tool 5 generates the GND connection area.

![](../images/tarea9/t9-09.jpg)

Final PCB design for the ESP32 microcontroller.

![](../images/tarea9/t9-10.jpg)

PCB with the soldered components.

Next, I designed a PCB with 5 push buttons and a PCB to connect servo motors.

### PCB for 5 push buttons

![](../images/tarea9/t9-11.jpg)

Schematic circuit for 5 buttons.

![](../images/tarea9/t9-12.jpg)

Circuit board mode for 5 push buttons with unordered components.

![](../images/tarea9/t9-13.jpg)

Final PCB design for 5 push buttons.

![](../images/tarea9/t9-14.jpg)

PCB with soldered components.

### PCB for servo motors.

![](../images/tarea9/t9-15.jpg)

Schematic circuit for servo motors.

![](../images/tarea9/t9-16.jpg)

Circuit board mode for servo motors with unordered components.

![](../images/tarea9/t9-17.jpg)

Final PCB layout for servo motors.

![](../images/tarea9/t9-18.jpg)

PCB with soldered components.

## Useful links

- [Eagle](https://www.autodesk.com/products/eagle/overview?term=1-YEAR&tab=subscription)

## Files

- [PCB para ESP32](../images/tarea10/files/pcb-esp32.rar)

- [PCB control por pulsadores](../images/tarea10/files/pcb-pulsador.rar)

- [PCB conexion de servo motores](../images/tarea10/files/pcb-servo.rar)
