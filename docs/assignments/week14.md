# 14. Project development

The design and fabrication of the complete mechanism of Robot Arm in 3D printer and control by IP link through PCB have been completed, the servo motors have been assembled and connected to the microcontroller circuit, in the same way it was culminated with the control program of the servo motors with IP communication.

![](../images/tarea13/t13-00.jpg)

Base design.

![](../images/tarea13/t13-01.jpg)

Arm design.

![](../images/tarea13/t13-02.jpg)

Gripper design.

![](../images/tarea13/t13-03.jpg)

Configuration and slice for 3D printing.

![](../images/tarea13/t13-04.jpg)

Cutting operation configuration for the laser cutter.

![](../images/tarea13/t13-05.jpg)

PCB microcontroller design.

![](../images/tarea13/t13-06.jpg)

PCB and component fabrication.

![](../images/tarea13/t13-07.jpg)

Program for control via IP link.

![](../images/tarea13/t13-08.jpg)

Robot arm fully assembled and connected.

<iframe width="1250" height="500" src="https://www.youtube.com/embed/aHxryWZ8iDQ" title="Brazo Robot Final" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Operation of Robot Arm.

## Files

- [Archicvos Diseño mecanico - Parte 1](../images/tarea13/files/Diseño_brazo_robot_01.rar)

- [Archicvos Diseño mecanico - Parte 2](../images/tarea13/files/Diseño_brazo_robot_02.rar)

- [Archivo impresion 3D](../images/tarea13/files/CCR20PRO_adaptador_pinza_base.3mf)

- [Archivos PCB para ESP32](../images/tarea10/files/pcb-esp32.rar)

- [Archivos PCB conexion de servo motores](../images/tarea10/files/pcb-servo.rar)

- [Archivo Codigo control IP](../images/tarea13/files/Brazo_control_bluetooth_IP.ino)

