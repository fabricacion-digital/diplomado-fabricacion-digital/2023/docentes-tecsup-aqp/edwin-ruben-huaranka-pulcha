# 4. Computer controlled cutting

## Laser cutting equipment

To carry out this activity we used HSG HS-Z1390 laser cutting equipment.

Different tests were carried out to identify the cutting precision of the equipment in different materials.

### Focus Characterization

First, a measurement of the laser focus height was performed, using a triangular ruler to set different test heights.

![](../images/tarea3/t3-00.jpg)

Focus height gauge for setting heights.

![](../images/tarea3/t3-01.jpg)

Focus Height Adjustment Procedure

![](../images/tarea3/t3-02.jpg)

Evaluation of Results, top face

![](../images/tarea3/t3-03.jpg)

Evaluation of Results, back face

According to the focus test, it was observed that at a distance between 4 mm and 5 mm the hole presents a more rounded shape both on the upper and rear face; after comparing both holes, it was considered that at a height of 4 mm the ideal cut would be the one with the smaller diameter and more rounded shape.

### Power and Speed to Cut Characterization

To configure the cutting parameters, we worked with the RDworks software. The configuration of these parameters: speed and power, are made with the selection of a specific color in the software.

#### Test of MDF 4mm

![](../images/tarea3/t3-04.jpg)

Design for cutting MDF 4mm with speeds of 10, 35, 60, 60, 85 and 110 mm/s and powers of 10%, 30%, 45%, 60% and 80%.

![](../images/tarea3/t3-05.jpg)

Test of speed and power of MDF 4mm.

> Results of the MDF 4mm test: It is observed that the cut is achieved with the lowest speed, which is 10 mm/s and with powers from 45% to more.

#### Test of ACRYLIC 4mm

![](../images/tarea3/t3-06.jpg)

Design for cutting ACRYLIC 4mm with speeds of 10, 35, 60, 60, 85 and 110 mm/s and powers of 10%, 30%, 45%, 60% and 80%.

![](../images/tarea3/t3-07.jpg)

Test of speed and power of ACRYLIC 4mm.

> Results of the ACRYLIC 4mm test: It is observed that the cut is achieved with the lowest speed, which is 10 mm/s and with powers from 45% to more.

#### Test of MDF 5mm

![](../images/tarea3/t3-08.jpg)

Design for cutting MDF 5mm with speeds of 10, 35, 60, 60, 85 and 110 mm/s and powers of 10%, 30%, 45%, 60% and 80%.

![](../images/tarea3/t3-09.jpg)

Test of speed and power of MDF 4mm.

> Results of the MDF 5mm test: It is observed that the cut is achieved with the lowest speed, which is 10 mm/s and with a high power of 80%.

### Press-fit test on MDF 3mm

A test fit was performed to determine the exact dimension in which the cut pieces would fit.

A sketch with test dimensions was designed in Fusion 360 software, then configured in RDworks to be cut on the laser cutting equipment.

![](../images/tarea3/t3-10.jpg)

Sketch design in Fusion 360 part 1.

![](../images/tarea3/t3-11.jpg)

Sketch design in Fusion 360 part 2.

![](../images/tarea3/t3-12.jpg)

Parameters configuration in RDworks

![](../images/tarea3/t3-13.jpg)

Cutting and press fit test.

> The result of the test was that the best press fit in 3mm MDF is in the 2.7 mm dimension.

### Design and laser cut assembled part

The design of an airplane with exploded view was made in CorelDraw software, the cutting parameters were configured in RDworks, the cutting was made in the laser equipment and the components were assembled.

![](../images/tarea3/t3-14.jpg)

Part design in CorelDraw

![](../images/tarea3/t3-15.jpg)

Configuration of cutting parameters in RDworks.

![](../images/tarea3/t3-16.jpg)

Cutting result without assembly

![](../images/tarea3/t3-17.jpg)

Assembled airplane with correct fit

## Vinyl cutting equipment

To carry out this activity we used Roland GX-24 vinyl cutting equipment.

### Cut something on the vinylcutter

The design of an anime logo was made in CorelDraw, the cutting lines were configured in the CutStudio program for each color of vinyl used, we proceeded to make the cut with the vinyl cutting equipment and finally with the help of an already cut acrylic base the logo was glued.

![](../images/tarea3/t3-18.jpg)

Logo design and color separation in CorelDraw.

![](../images/tarea3/t3-19.jpg)

Cutting configuration in CutStudio program with blue color.

![](../images/tarea3/t3-20.jpg)

Cutting configuration in CutStudio program with black color.

![](../images/tarea3/t3-21.jpg)

Cutting settings in CutStudio program with white color.

<iframe width="1271" height="500" src="https://www.youtube.com/embed/_Hs86OtLQqg" title="VID 20230804 160159" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
 
Cutting process on Roland GX-24.

![](../images/tarea3/t3-22.jpg)

Vinyl already cut ready to be glued on its base.

![](../images/tarea3/t3-23.jpg)

Final logo adhered to its base.

![](../images/tarea3/t3-24.jpg)

The work was successfully completed!


## Useful links

- [CorelDraw](https://www.coreldraw.com/la/)

- [Fusion 360](https://www.autodesk.com/products/fusion-360/overview?term=1-YEAR&tab=subscription)

- [RDworks](https://rdworkslab.com/)

- [CutStudio](https://www.rolanddga.com/es/productos/software/roland-cutstudio-software)


## Files

- [Speed and power test](../images/tarea3/files/laser-test.rar)

- [Press-fit test](../images/tarea3/files/test-measure.rar)

- [Assembled part](../images/tarea3/files/assembled-part.rar)

- [Vinyl cutting](../images/tarea3/files/vinyl-cutting.rar)