# 10. Electronics production

## Characterize the design rules for the internal production process of printed circuit boards.

In this activity it is important to perform etching tests with the available milling cutters, this will allow us to know the track and etching thicknesses that we can perform on our board designs. For them we made a test design:

![](../images/tarea10/t10-00.jpg)

I designed a test PCB with different dimensions, this board shows track width and etch width ranging from 0.001 mills to 0.02 mills.

![](../images/tarea10/t10-01.jpg)

V-shaped tool with 30° and diameter of 0.1mm diameter.

![](../images/tarea10/t10-02.jpg)

PCB test fabrication with V-shaped tool with 30° and 0.1mm diameter.

> It was observed that the 0.02 mills track and the 0.02 mills groove were machined satisfactorily, so with the selected tool we can manufacture tracks of this dimension and be able to work correctly.

## Create a PCB that includes a microcontroller with assisted manufacturing.

In this task the research focused on PCB fabrication, mainly on the use of the FlatCam software and its respective configuration, as well as characterizing the lab's monofab to know its limitations.

I designed my own PCB with the considerations identified in my test circuit.

To generate the PCB fabrication commands I used the FlatCam software, the user interface is simple; the dimensions of the cutting tool to be used must be configured, in this case a V-shaped milling cutter with 30° and 0.1 mm; in the same way the desired cutting depth must be configured and then the PCB cut for its removal. After obtaining the .nc files, they are uploaded to the monofab equipment and the PCB fabrication begins.

![](../images/tarea10/t10-03.jpg)

Schematic drawing of circuits in Eagle Software.

![](../images/tarea10/t10-04.jpg)

Design of my PCB for ESP32 and obtaining .gbr files from Eagle.

![](../images/tarea10/t10-05.jpg)

Generation of machining path in FlatCAM and .nc files for monoFAB.

<iframe width="1250" height="500" src="https://www.youtube.com/embed/tsW_bRdvov8" title="PCB Cam" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

PCB Fabrication in monoFAB CNC

![](../images/tarea10/t10-06.jpg)

Fully machined PCB.

![](../images/tarea10/t10-07.jpg)

Soldered components on the created PCB.

In addition to the PCB for the ESP32, two secondary PCBs were fabricated for pushbutton control and servo motor connection.

### PCB for pushbutton control

![](../images/tarea10/t10-08.jpg)

Schematic drawing of circuits in Eagle Software.

![](../images/tarea10/t10-09.jpg)

Design of my PCB for ESP32 and obtaining .gbr files from Eagle.

![](../images/tarea10/t10-10.jpg)

Generation of machining path in FlatCAM and .nc files for monoFAB.

![](../images/tarea10/t10-11.jpg)

Fully machined PCB.

![](../images/tarea10/t10-12.jpg)

Soldered components on the created PCB.

### PCB for servo motor connection

![](../images/tarea10/t10-13.jpg)

Schematic drawing of circuits in Eagle Software.

![](../images/tarea10/t10-14.jpg)

Design of my PCB for ESP32 and obtaining .gbr files from Eagle.

![](../images/tarea10/t10-15.jpg)

Generation of machining path in FlatCAM and .nc files for monoFAB.

![](../images/tarea10/t10-16.jpg)

Fully machined PCB.

![](../images/tarea10/t10-17.jpg)

Soldered components on the created PCB.

## Problems - Troubleshooting.

- When manufacturing the PCB one of the connection points was not machined, which generated a power crossover; it was solved by unsoldering the component from that point and manually correcting the board.

- When soldering the LED was placed in the opposite direction, it was solved by desoldering and positioning it in the correct way.

- A first PCB was made for the pushbutton control, which generated a connection problem with GND; it was decided to make another one with a different design, which was used at the end.

![](../images/tarea10/t10-18.jpg)

Location of the solved components

![](../images/tarea10/t10-19.jpg)

PCB for pushbutton control failed

## Useful links

- [Eagle](https://www.autodesk.com/products/eagle/overview?term=1-YEAR&tab=subscription)

- [FlatCAM](http://flatcam.org/download)

## Files

- [PCB prueba dimensional](../images/tarea10/files/pcb-dimension.rar)

- [PCB para ESP32](../images/tarea10/files/pcb-esp32.rar)

- [PCB control por pulsadores](../images/tarea10/files/pcb-pulsador.rar)

- [PCB conexion de servo motores](../images/tarea10/files/pcb-servo.rar)
