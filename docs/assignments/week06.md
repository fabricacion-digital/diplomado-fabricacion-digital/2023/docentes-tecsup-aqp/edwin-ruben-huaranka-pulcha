# 6. Computer controlled machining

## Dimensional cutting verification

A design of 3 components for a socket assembly was made, in order to test the proper dimensions and some other considerations to manufacture a piece of furniture without any inconvenience.

![](../images/tarea5/t5-00.jpg)

Part design in Fusion 360

<iframe width="1271" height="500" src="https://www.youtube.com/embed/RLbyPO3feI0" title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Machining path configuration and simulation in Fusion 360

![](../images/tarea5/t5-01.jpg)

Conversion of Fusion 360 G-code to G-code suitable for the equipment with CNC_Change software.

<iframe width="1271" height="500" src="https://www.youtube.com/embed/asA6q2UQnnc" title="VID 20230810 151928" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Machining of parts on cutting equipment.

![](../images/tarea5/t5-02.jpg)

Failed socket test. Tool impact at start and end of machining.

![](../images/tarea5/t5-03.jpg)

Proper socket test.

> Results obtained: When performing the socket test with the designed components it was concluded that it has an offset of 1 mm for both external and internal dimensions, also when generating the machining code an error appears at the beginning and end which impacts the cutting tool with the material.

> Problems and solution: As mentioned in the results there were problems in the start and end machining, this was caused due to a configuration of the G code, the solution that was applied was to modify the same G code in such a way that in the Z coordinate a greater height was given so that in this way the tool does not impact.

## Equipment operation

The cutting equipment to be used is SUDA model VG 1325. In order to operate this equipment it is necessary to use a key control with which the whole process is configured: zero point setting, selection of the file to be machined and verification of parameters.

![](../images/tarea5/t5-04.jpg)

Zero point setting in Z by means of a sensor.

![](../images/tarea5/t5-05.jpg)

Zero point setting in X/Y by means of a button on the control.

![](../images/tarea5/t5-06.jpg)

File selection to be machined and start of machining.

## Machining path and parameters configuration in Fusion 360

To machine any part, you must first set up the path in Fusion 360. For this configuration it is necessary to know different parameters, in this way the machining is done correctly.

- First we configure the base material to be machined and specify the zero point of the process.

![](../images/tarea5/t5-07.jpg)

Initial configuration of the material.

- Then the machining operations are configured. In this case we work with the "Contouring" operation where the internal zones of the parts are selected so that no problems are generated later. In this operation the RPM, feed rate, tool penetration depth and all machining parameters are configured.

![](../images/tarea5/t5-08.jpg)

Machining path configuration for internal zones.

![](../images/tarea5/t5-09.jpg)

Machining path configuration for external zones.

- Once all the operations are configured, we proceed to postprocess to obtain the G code, in this code is all the automatically generated path.

## Furniture manufacturing

After learning how the equipment works and how to generate the machining route, we made the manufacture of real size furniture for its respective use.

### Dog feeder

![](../images/tarea5/t5-10.jpg)

Design in Fusion 360.

<iframe width="1271" height="500" src="https://www.youtube.com/embed/RLbyPO3feI0" title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Machining path configuration in Fusion 360.

![](../images/tarea5/t5-11.jpg)

G-code conversion in CNC_Change.

<iframe width="1271" height="500" src="https://www.youtube.com/embed/YdwQoMlff4s" title="VID 20230811 122358" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Machining process of the dog feeder.

![](../images/tarea5/t5-12.jpg)

Cut pieces of the dog feeder

![](../images/tarea5/t5-13.jpg)

Assembled dog feeder

### Night table

![](../images/tarea5/t5-14.jpg)

Design in Fusion 360.

<iframe width="1271" height="500" src="https://www.youtube.com/embed/G-Te5b4bnkQ" title="Mesa de noche" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Machining path configuration in Fusion 360.

![](../images/tarea5/t5-15.jpg)

G-code conversion in CNC_Change.

![](../images/tarea5/t5-16.jpg)

Night table cut parts

![](../images/tarea5/t5-17.jpg)

Assembled night table

![](../images/tarea5/t5-18.jpg)

Both jobs were successfully completed! And they can be used at home :D


## Useful links

- [Fusion 360](https://www.autodesk.com/products/fusion-360/overview?term=1-YEAR&tab=subscription)

- [CNC_Change](https://cnc-change.updatestar.com/)


## Files

- [Socket test](../images/tarea5/files/test.rar)

- [Dog feeder](../images/tarea5/files/feeder.rar)

- [Night table](../images/tarea5/files/night-table.rar)
