# 3. Computer Aided design

## 2D software evaluation and selection

To perform this task I considered two 2D design software: CorelDraw and Inkscape.

In general terms both software have similar characteristics, the only difference between them and "advantage" for Inkscape software is that it is a free software.

### CorelDraw Software

We start the CorelDraw software

![](../images/tarea2/t2-00.jpg)

We create a new file for the design.

![](../images/tarea2/t2-01.jpg)

The tools to use are: "Pen" and "Shape". These tools will be used to draw the lines, modify them as curves and designate color.

![](../images/tarea2/t2-03.jpg)

![](../images/tarea2/t2-04.jpg)

Final sketch based on the photo of the possible proposed final project.

![](../images/tarea2/t2-05.jpg)

### Inkscape Software

We start the Inkscape software and create a new file for the design.

![](../images/tarea2/t2-06.jpg)

The tools to be used are very similar to the CorelDraw software icon. These tools will be used to draw the lines, modify them as curves and designate color.

![](../images/tarea2/t2-07.jpg)

![](../images/tarea2/t2-08.jpg)

Final sketch based on the photo of the possible final project proposed.

![](../images/tarea2/t2-09.jpg)

## 3D software evaluation and selection

To perform this task I considered two 3D design software: Solidworks and Inventor.

Both software have different characteristics from each other, but in terms of 3D application both can perform components, assemblies and simulations in a fast and efficient way when you have some mastery of their use.

### Solidworks

Open SOLIDWORKS software

![](../images/tarea2/t2-10.jpg)

Arm sketch 01

![](../images/tarea2/t2-11.jpg)

Extrusion of the created sketch

![](../images/tarea2/t2-12.jpg)

Finished component with all operations

![](../images/tarea2/t2-13.jpg)

Insertion of created components for assembly

![](../images/tarea2/t2-14.jpg)

Final assembly of the robot arm in Solidworks

- Component creation video

<iframe width="1271" height="500" src="https://www.youtube.com/embed/TXJNmyM3kOI" title="Brazo robot componentes Solid" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

- Robot arm animation video

<iframe width="1271" height="500" src="https://www.youtube.com/embed/WwxdQD22Vtc" title="Brazo robot ensamble Solid" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### Inventor

Open INVENTOR software

![](../images/tarea2/t2-15.jpg)

Arm sketch 01

![](../images/tarea2/t2-16.jpg)

Extrusion of the created sketch

![](../images/tarea2/t2-17.jpg)

Finished component with all operations

![](../images/tarea2/t2-18.jpg)

Insertion of created components for assembly

![](../images/tarea2/t2-19.jpg)

Final assembly of the robot arm in Inventor

- Component creation video

<iframe width="1271" height="500" src="https://www.youtube.com/embed/uQdaiWA5wIM" title="Brazo robot componentes Inventor" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

- Robot arm animation video

<iframe width="1271" height="500" src="https://www.youtube.com/embed/UfGXtfcTqfE" title="Brazo robot ensamble Inventor" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## 3d view of the final robot arm assembly

<div class="sketchfab-embed-wrapper"> <iframe title="Brazo Robot" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share width="640" height="480" src="https://sketchfab.com/models/fa0a11c927f2445a954a3aafb03f2f4c/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/brazo-robot-fa0a11c927f2445a954a3aafb03f2f4c?utm_medium=embed&utm_campaign=share-popup&utm_content=fa0a11c927f2445a954a3aafb03f2f4c" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> Brazo Robot </a> by <a href="https://sketchfab.com/ehuaranka?utm_medium=embed&utm_campaign=share-popup&utm_content=fa0a11c927f2445a954a3aafb03f2f4c" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> ehuaranka </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=fa0a11c927f2445a954a3aafb03f2f4c" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>

## Useful links

- [CorelDraw](https://www.coreldraw.com/la/)

- [Inkscape](https://inkscape.org/es/)

- [Solidworks](https://www.solidworks.com/es)

- [Inventor](https://www.autodesk.com/products/inventor/overview?term=1-YEAR&tab=subscription&plc=INVPROSA)

## Files

### 2D files

- [Possible project file in CorelDraw](../images/tarea2/files/tarea2-corel2d.cdr)

- [Possible project file in Inkscape](../images/tarea2/files/tarea2-inkscape2d.svg)

### 3D files

- [Possible project file in Inventor Professional](../images/tarea2/files/brazo-robot-inventor.rar)

- [Possible project file in Solidworks](../images/tarea2/files/brazo-robot-solidworks.rar)