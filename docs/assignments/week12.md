# 12. Invention, intellectual property and income

## Develop a plan for dissemination of your final project

### Invention

The name of my project is "TEC Robot Arm", it is only for educational purposes for its application to the teaching of my students.

For now, the project is intended to be an educational module for higher technical education students to develop skills in the use of 3D and 2D design software, digital fabrication equipment such as 3D printers and laser cutters, electronic circuits, microcontroller programming, actuator management and wireless networks with IP link poe.

### License

As this is a purely academic project, I consider that the license would be

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://fabricacion-digital.gitlab.io/diplomado-fabricacion-digital/2023/docentes-tecsup-aqp/edwin-ruben-huaranka-pulcha/assignments/week13/">Brazo robot Tec</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://fabricacion-digital.gitlab.io/diplomado-fabricacion-digital/2023/docentes-tecsup-aqp/edwin-ruben-huaranka-pulcha/">EHuaranka</a> is licensed under <a href="http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"></a></p>

What does this mean?

CC BY: This license allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, provided the creator is cited. The license allows commercial use.

How can I obtain the license?

![](../images/tarea12/t12-00.jpg)

Start to acquire a license.

![](../images/tarea12/t12-01.jpg)

Characteristics of the selected license.

![](../images/tarea12/t12-02.jpg)

Here we can configure the authorship acknowledgment as well as the code to insert the license in the web page.

## Prepare drafts of your summary slide (presentation.png, 1920x1080) and video clip ( presentation.mp4, 1080p HTML5, < ~minute, < ~10 MB) and put them in your root directory

![](../images/tarea12/files/slide_robot_arm_tec.png)

Slide of the project.

<iframe width="1250" height="500" src="https://www.youtube.com/embed/q-2qHyKUENM" title="Video presentacion Robot Arm" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Video presentation of the project.

## Useful links

- [Creative Commons](https://creativecommons.org/licenses/by/4.0/)

## Files

- [Final project slide](../images/tarea12/files/slide_robot_arm_tec.png)

