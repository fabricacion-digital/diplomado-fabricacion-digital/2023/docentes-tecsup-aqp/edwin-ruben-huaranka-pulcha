# 13. Project presentations

## What does it do?

The project is a 3D printed robot arm that performs its movements with wireless control.

First, the operator through a mobile device such as a cell phone or tablet has access to a link IP address, in this IP address is displayed an interface to control the movements of the arm by sliding it on the screen, this information is captured by the digital analog converters of the microcontroller and finally sent to the servo motors of the robot arm, thus allowing the controlled movement of each part of the arm. 

## Who's done what beforehand?

In different design sites you can find sketches or digital models of robot arms, ready to assemble arms and there are arms for manufacturing such as the Kuka brand.

- [Kuka industrial robot](https://www.kuka.com/es-es/productos-servicios/sistemas-de-robot/robot-industrial)

- [Acrylic mini robotic arm](https://mtlab.pe/producto/brazo_robotico_acrilico/)

In my case, the project is educational, the arm I make will be a way of teaching students to see how to link digital manufacturing with physics, and in turn how to give an electronic control for a specific operation.

## What did you design?

For the control system:

- I designed the microcontroller connection PCB.
- I designed the servo motor connection PCB.
- I developed the linking program between the interface and the microcontroller.

For the mechanical system:

- I designed the base, arm, forearm and gripper.
- I designed the housing for the servo motors.
- I designed the support for the robot arm.

## What materials and components were used?

Materials 

| Quantity | Material | Description |
| --- | --- | --- |
| 01 | 3D Filament | PLA - 51 grams |
| 01 | Acrylic sheet | 15 cm x 15 cm |

Components

| Quantity | Component | Description |
| --- | --- | --- |
| 01 | Microcontroller | ESP32 |
| 04 | Servo motors | SG90 |
| 01 | Power supply | 5Vdc power supply |

## Where did they come from?

All the materials and components used were provided by the person in charge of teaching.

## How much did they cost?

Materials 

| Quantity | Material | Description | Cost |
| --- | --- | --- | --- |
| 01 | 3D Filament | PLA | $ 25.00 |
| 01 | Acrylic | 15 cm x 15 cm | $ 15.00 |

Components

| Quantity | Component | Description | Cost |
| --- | --- | --- | --- |
| 01 | Microcontroller |ESP32 | $ 10.00 |
| 04 | Servomotors | SG90 | $ 100.00 |
| 01 | Power supply | 5Vdc power supply | $ 12.50 |

## What parts and systems were made?

3D printer fabrication:

- Robot arm parts: base, arm, forearm and gripper.

Manufacture in laser cutter:

- Support for robot arm.

Desktop CNC manufacturing:

- PCB for microcontroller connection.
- Servo motor connection PCB.

## What processes were used?

For the robot arm project, additive manufacturing processes were used: 3D printing, and subtractive manufacturing processes: laser cutting and CNC engraving.

## What questions were answered?

This is the first time I work with microcontrollers, IP link systems and wireless operation; when I started I thought it was a very complex system to make, but with the study I came to the conclusion that it was not so complicated if you understand the basic concepts.

The movement of the servo motors with the first printed model turned out to be a bit jerky or slow in certain situations, this is because it was necessary to optimize based on the torque of the motor the weight it will move; it could be optimized by modifying the design and reducing the density of the printed parts.

## What worked? What didn't?

- The IP interface control of the servo motors works correctly, in the same way the mechanical part in conjunction with the servo motors have the right sets and settings for ideal operation; in short, everything manufactured works as intended.

- Initially a Bluetooth link was tested, but it was discarded because after a period of time of operation the wireless connection was lost and a reset to the whole control had to be performed.

## How was it evaluated?

I performed grip tests with different objects such as:

- A USB memory.
- A pen cap.
- A milling cutter container.

## What are the implications?

With this project students will be able to learn the integration of digital fabrication, physical fabrication and electronic control in the same project; they will also be able to learn how to generate programming for the control of servomotors with both pushbutton control and IP interface link control.
