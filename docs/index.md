# Home

## Hello!

![](.\images\Images-page\Foto-perfil.jpg){width=405, length=477}

Hi! I am Edwin Huaranka. I am a machine maintenance technician and future mechanical engineer living in Peru. I work in education, consulting, service industry and I am entering the world of digital manufacturing.

Visit this website to see my work!

## Welcome to your new Fab Academy site

This is the Fab Academy site of Edwin Huaranka. Enjoy it!.

----
