
#include <ESP32Servo.h>
Servo servo1;               //base
Servo servo2;               //brazo
Servo servo3;               //antebrazo
Servo servo4;               //garra

int pin_izquierda = 16;
int pin_derecha =   4;
int pin_arriba =    18;
int pin_abajo =     5;
int pin_garra =     17;

int var_izquierda = 0;
int var_derecha = 0;
int var_arriba = 0;
int var_abajo = 0;
int var_garra = 0;
boolean flag_garra = false;

int pin_servo1 = 13;
int pin_servo2 = 12;
int pin_servo3 = 14;
int pin_servo4 = 27;

int pos_servo1 = 90;
int pos_servo2 = 90;
int pos_servo3 = 90;
int pos_servo4 = 0;

int tiempo_servo = 20;

void setup() {
  Serial.begin(9600);           //Iniciamos comunicación

  pinMode(pin_izquierda, INPUT); 
  pinMode(pin_derecha, INPUT);
  pinMode(pin_arriba, INPUT);
  pinMode(pin_abajo, INPUT);
  pinMode(pin_garra, INPUT); 
  
  pinMode(pin_servo1, OUTPUT);
  pinMode(pin_servo2, OUTPUT);
  pinMode(pin_servo3, OUTPUT);
  pinMode(pin_servo4, OUTPUT);
  servo1.attach(pin_servo1);
  servo2.attach(pin_servo2);
  servo3.attach(pin_servo3);
  servo4.attach(pin_servo4);
  
  servo1.write(pos_servo1);
  servo2.write(pos_servo2);
  servo3.write(pos_servo3);
  servo4.write(pos_servo4);        //Iniciamos con la garra abierta
  delay(2000);
}

void loop() {

  var_izquierda = digitalRead(pin_izquierda);
  var_derecha = digitalRead(pin_derecha);
  var_arriba = digitalRead(pin_arriba);
  var_abajo = digitalRead(pin_abajo);
  var_garra = digitalRead(pin_garra);

  if(var_izquierda == HIGH){
    izquierda();  
  }
  if(var_derecha == HIGH){
    derecha();  
  }
  if(var_arriba == HIGH){
    arriba();  
  }
  if(var_abajo == HIGH){
    abajo();  
  }

  if(var_garra == HIGH && flag_garra == false){
    cerrar_garra();  
    flag_garra = true;
  }

  if(var_garra == HIGH && flag_garra == true){
    abrir_garra();  
    flag_garra = false;
  }
 
  delay(50);
 
  }
  
  void izquierda(){
    servo1.write(pos_servo1);
    delay(tiempo_servo);
    pos_servo1 = pos_servo1 + 1; 
    if(pos_servo1>=180){
      pos_servo1 = 180;
    }
     
  }
  
  void derecha(){
    servo1.write(pos_servo1);
    delay(tiempo_servo);
    pos_servo1 = pos_servo1 - 1;
    if(pos_servo1<=0){
      pos_servo1 = 0;
    }
  }
  
  void arriba(){
    servo2.write(pos_servo2);
    servo3.write(pos_servo3);
    delay(tiempo_servo);
    pos_servo2 = pos_servo2 + 1;
    pos_servo3 = pos_servo3 - 1;
    if(pos_servo2>=120){
      pos_servo2 = 120;
      pos_servo3 = 60;
    }
  }
  
  void abajo(){
    servo2.write(pos_servo2);    
    delay(tiempo_servo);
    pos_servo2 = pos_servo2 - 1;
    if(pos_servo2<=0){
      pos_servo2 = 0;  
    }    
  }
  
  void abrir_garra(){   
    while(var_garra == HIGH){
     servo4.write(pos_servo4);
     delay(tiempo_servo);
     pos_servo4 = pos_servo4 - 1;
     var_garra = digitalRead(pin_garra);
     if(pos_servo4<=0){
      pos_servo4 = 0;
     } 
    }
  }
  
  void cerrar_garra(){    
    while(var_garra == HIGH){                        
     servo4.write(pos_servo4);
     delay(tiempo_servo);
     pos_servo4 = pos_servo4 + 1;
     var_garra = digitalRead(pin_garra);  
     if(pos_servo4>=180){
      pos_servo4 = 180;
     }   
    }   
  }
